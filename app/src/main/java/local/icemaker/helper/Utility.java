package local.icemaker.helper;

import android.content.Context;
import android.telephony.SmsManager;
import android.widget.Toast;

import local.icemaker.model.Sms;

public class Utility {
    public void makeToast(Context context, String msg){
        Toast.makeText(context, msg,
                Toast.LENGTH_SHORT).show();
    }

    public boolean sendSms(Context context, Sms sms){
        try{
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(String.valueOf(sms.getOutgoingPhoneNum()), null, sms.getMsg(), null, null);
            Toast.makeText(context, "ICE Notification Sent Successfully!",
                    Toast.LENGTH_LONG).show();
            return true;
        } catch (Exception e){
            Toast.makeText(context,
                    "Unable to send ICE notification :(",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return false;
        }
    }
}
