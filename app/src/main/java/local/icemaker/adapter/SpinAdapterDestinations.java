package local.icemaker.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import local.icemaker.model.Destination;

public class SpinAdapterDestinations extends ArrayAdapter<Destination>{

    private Context _context;
    private Destination[] _values;

    public SpinAdapterDestinations(Context context, int textViewResourceId, Destination[] values){
        super(context, textViewResourceId, values);
        this._context = context;
        this._values = values;
    }

    public int getCount(){
        return this._values.length;
    }

    public Destination getItem(int position){
        return this._values[position];
    }

    public long getItemId(int position){
        return position;
    }
}
