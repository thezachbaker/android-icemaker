package local.icemaker.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import local.icemaker.model.Direction;

public class SpinAdapterDirections extends ArrayAdapter<Direction>{

    private Context _context;
    private Direction[] _values;

    public SpinAdapterDirections(Context context, int textViewResourceId, Direction[] values){
        super(context, textViewResourceId, values);
        this._context = context;
        this._values = values;
    }

    public int getCount(){
        return this._values.length;
    }

    public Direction getItem(int position){
        return this._values[position];
    }

    public long getItemId(int position){
        return position;
    }
}
