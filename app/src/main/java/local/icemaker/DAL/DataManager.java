package local.icemaker.DAL;

import local.icemaker.model.Beacon;
import local.icemaker.model.Destination;
import local.icemaker.model.DestinationArea;

public class DataManager {

    // Determines if the discovered beacon is for the ICEMaker app or not
    public boolean BeaconValidCheck(Beacon beacon)
    {
        return true;
    }

    public DestinationArea GetCurrentLocationDetails(Beacon beacon)
    {
        DestinationArea destinationArea = new DestinationArea(
                "Alderaan National Park"
                );

        destinationArea.addDestination(new Destination(
                001,
                "Crater Trailhead"
        ));
        destinationArea.addDestination(new Destination(
            002,
            "Rock Belt Creek"
    ));
        destinationArea.addDestination(new Destination(
                003,
                "Hopeless Canyon"
        ));

        return destinationArea;
    }

    public void LogTrip(Beacon beacon, Destination destination)
    {
        //TODO: Write the notification as a trip logged
    }
}
