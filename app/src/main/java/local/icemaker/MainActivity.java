package local.icemaker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import local.icemaker.form.FormSendNotification01Destination;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get view elems
        Button btnSendNotificationFormActivity = (Button) findViewById(R.id.btnSendNotification);
        Button btnSetDefaultContactActivity = (Button) findViewById(R.id.btnSetDefaultContact);
        Button btnWatchTutorialActivity = (Button) findViewById(R.id.btnWatchTutorial);

        View.OnClickListener onClick_btnSendNotificationFormActivity = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), FormSendNotification01Destination.class);
                startActivity(intent);
                overridePendingTransition(R.animator.push_left_in, R.animator.push_left_out);
            }
        };

        View.OnClickListener onClick_btnSetDefaultContactActivity = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), FormSetDefaultContactActivity.class);
                startActivity(intent);
            }
        };

        View.OnClickListener onClick_btnWatchTutorialActivity = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), VideoTutorialActivity.class);
                startActivity(intent);
            }
        };

        btnSendNotificationFormActivity.setOnClickListener(onClick_btnSendNotificationFormActivity);
        btnSetDefaultContactActivity.setOnClickListener(onClick_btnSetDefaultContactActivity);
        btnWatchTutorialActivity.setOnClickListener(onClick_btnWatchTutorialActivity);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.animator.push_left_out, R.animator.push_left_in);
    }
}
