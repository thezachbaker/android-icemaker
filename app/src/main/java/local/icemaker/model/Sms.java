package local.icemaker.model;

/**
 * Created by zach.baker on 10/28/2015.
 */
public class Sms {
    private int _outgoingPhoneNum;
    private String _msg;

    public Sms(int outgoingPhoneNum, String msg){
        this._outgoingPhoneNum = outgoingPhoneNum;
        this._msg = msg;
    }

    // Getters
    public int getOutgoingPhoneNum(){
        return this._outgoingPhoneNum;
    }
    public String getMsg(){
        return this._msg;
    }
}
