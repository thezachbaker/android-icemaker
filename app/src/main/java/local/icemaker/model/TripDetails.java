package local.icemaker.model;

public class TripDetails {
    private int _id;
    private double _duration;
    private String _direction;
    private String _comment;

    public TripDetails(){
    }

    // Setters
    public void setId(int id){
        this._id = id;
    }
    public void setDuration(double duration){
        this._duration = duration;
    }
    public void setDirection(String direction){
        this._direction = direction;
    }
    public void setComment(String comment){
        this._comment = comment;
    }

    // Getters
    public double getDuration(){
        return this._duration;
    }
    public String getComment(){
        if(this._comment != null && !this._comment.isEmpty()){
            return this._comment;
        } else {
            return "";
        }
    }
}
