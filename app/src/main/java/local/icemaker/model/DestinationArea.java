package local.icemaker.model;

import java.util.List;

public class DestinationArea {
private int _id;
private String _name;
private List<Destination> destinations;

    public DestinationArea(String name)
    {
        this._name = name;
    }

    // Setters
    public void setId(int id){this._id = id;}
    public void setName(String name)
    {
        this._name = name;
    }

    // Getters
    public int getId(){
        return this._id;
    }
    public String getName(){
        return this._name;
    }

    // Methods
    public void addDestination(Destination destination)
    {
        this.destinations.add(destination);
    }
}