package local.icemaker.model;

public class FormNotificationData {
    private static FormNotificationData instance = null;
    private Beacon _beacon;
    private Destination _destination;
    private DestinationArea _destinationArea;
    private Direction _direction;
    private Contact _contact;
    private TripDetails _tripDetails;

    // Empty constructor
    protected FormNotificationData(){
        this._tripDetails = new TripDetails();
    }

    // Setters
    public void setBeacon(Beacon beacon){
        this._beacon = beacon;
    }
    public void setDestination(Destination destination){
        this._destination = destination;
    }
    public void setDestinationArea(DestinationArea destinationArea){
        this._destinationArea = destinationArea;
    }
    public void setDuration(double duration){
        this._tripDetails.setDuration(duration);
    }
    public void setDirection(Direction direction){
        this._direction = direction;
    }
    public void setContact(Contact contact){
        this._contact = contact;
    }
    public void setComment(String comment){
        this._tripDetails.setComment(comment);
    }

    // Getters
    public static FormNotificationData getInstance() {
        if (instance == null) {
            instance = new FormNotificationData();
        }
        return instance;
    }
    public Destination getDestination(){
        return this._destination;
    }
    public DestinationArea getDestinationArea(){return this._destinationArea;}
    public double getDuration(){
        return this._tripDetails.getDuration();
    }
    public Direction getDirection(){
        return this._direction;
    }
    public Contact getContact(){
        return this._contact;
    }

    // Other Methods
    public Sms createSms(){
        String smsMsg = new String();
        String directionStr = this._direction.getName();
        double duration = this._tripDetails.getDuration();

        smsMsg += "I'm at " + this._destinationArea.getName() + ", ";
        smsMsg += "going to " + this._destination.getName() + ", ";

        switch(directionStr.toLowerCase()){
            case "staying put":
                smsMsg += "and I'm not planning to go to another area. ";
                break;
            case "not sure":
                smsMsg += "and I'm not sure which direction I'm headed when I get there. ";
                break;
            default:
                smsMsg += "headed " + directionStr + ". ";
                break;
        }

        smsMsg += "I should be gone for about " + String.valueOf(duration) + " hours. ";
        smsMsg += this._tripDetails.getComment();

        Sms sms = new Sms(this._contact.getPhoneNum(), smsMsg);

        return sms;
    }
}
