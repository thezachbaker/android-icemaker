package local.icemaker.model;

/**
 * Created by zach.baker on 10/27/2015.
 */
public class Direction {
    private int _id;
    private String _name;

    public Direction(int id, String name){
        this._id = id;
        this._name = name;
    }

    // Setters
    public void setId(int id){
        this._id = id;
    }
    public void setName(String name){
        this._name = name;
    }

    // Getters
    public int getId(){
        return this._id;
    }
    public String getName(){
        return this._name;
    }

    // Methods
    @Override
    public String toString(){
        return this._name;
    }
}
