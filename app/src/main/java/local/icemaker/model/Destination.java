package local.icemaker.model;

public class Destination {
    private int _id;
    private String _name;
    private String _auxName;

    public Destination(int id, String name)
    {
        this._id = id;
        this._name = name;
        this._auxName = null;
    }

    // Setters
    public void setId(int id){
        this._id = id;
    }
    public void setName(String name){
        this._name = name;
    }
    public void setAuxName(String name){
        this._auxName = name;
    }

    // Getters
    public int getId(){
        return this._id;
    }
    public String getName(){
        return this._name;
    }
    public String getAuxName(){
        return this._auxName;
    }

    // Override Methods
    @Override
    public String toString(){
        return this._name;
    }

    // Custom Methods
}
