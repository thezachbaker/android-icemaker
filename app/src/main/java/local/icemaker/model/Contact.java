package local.icemaker.model;

/**
 * Created by zach.baker on 10/27/2015.
 */
public class Contact {
    private int _id;
    private String _fName;
    private String _lName;
    private int _phoneNum;

    public Contact(String fName, String lName, int phoneNum){
        this._fName = fName;
        this._lName = lName;
        this._phoneNum = phoneNum;
    }

    // Setters
    public void setId(int id){
        this._id = id;
    }
    public void setFirstName(String name){
        this._fName = name;
    }
    public void setLastName(String name){
        this._lName = name;
    }
    public void setPhoneNum(int phoneNum){
        this._phoneNum = phoneNum;
    }

    // Getters
    public int getId(){
        return this._id;
    }
    public String getFirstName(){
        return this._fName;
    }
    public String getLastName(){
        return this._lName;
    }
    public int getPhoneNum(){
        return this._phoneNum;
    }
}
