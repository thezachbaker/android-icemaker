package local.icemaker.form;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import local.icemaker.R;
import local.icemaker.model.Contact;
import local.icemaker.model.Direction;
import local.icemaker.model.FormNotificationData;

public class FormSendNotification04ContactDetails extends FormActivityBase {

    private FormNotificationData formData = FormNotificationData.getInstance();

    private EditText fieldFirstName;
    private EditText fieldLastName;
    private EditText fieldPhoneNum;
    private Button btnNext;

    // Override methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_send_notification04_contact_details);

        fieldFirstName = (EditText) findViewById(R.id.form_send_notification_contact_firstname);
        fieldLastName = (EditText) findViewById(R.id.form_send_notification_contact_lastname);
        fieldPhoneNum = (EditText) findViewById(R.id.form_send_notification_contact_phone);

        btnNext = (Button) findViewById(R.id.form_send_notification_btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(validateForm()){
                    Intent intent = new Intent(v.getContext(), FormSendNotification05Comments.class);
                    startActivity(intent);
                    overridePendingTransition(R.animator.push_left_in, R.animator.push_left_out);
                }
            }
        });

        Direction direction = formData.getDirection();
        makeToast("Direction: " + direction.getName());
    }

    // Other methods
    private boolean validateForm(){
        String errorMsg = "";

        String providedFirstName = fieldFirstName.getText().toString();
        String providedLastName = fieldLastName.getText().toString();
        String providedPhoneNum = fieldPhoneNum.getText().toString();

        if(providedFirstName == null || providedFirstName.isEmpty()){
            errorMsg += getString(R.string.error_msg_invalid_contact_fname) + "\r\n";
        }

        if(providedLastName == null || providedLastName.isEmpty()){
            errorMsg += getString(R.string.error_msg_invalid_contact_lname) + "\r\n";
        }

        if(providedPhoneNum == null || providedPhoneNum.isEmpty()){
            errorMsg += getString(R.string.error_msg_invalid_contact_phone) + "\r\n";
        }

        if(errorMsg.length() > 0){
            makeToastError(errorMsg);
            return false;
        } else {
            Contact contact = new Contact(providedFirstName, providedLastName, Integer.parseInt(providedPhoneNum));
            formData.setContact(contact);
            return true;
        }
    }
}
