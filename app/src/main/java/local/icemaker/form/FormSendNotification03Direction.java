package local.icemaker.form;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import local.icemaker.R;
import local.icemaker.helper.Utility;
import local.icemaker.adapter.SpinAdapterDirections;
import local.icemaker.model.Direction;
import local.icemaker.model.FormNotificationData;

public class FormSendNotification03Direction extends FormActivityBase {

    private FormNotificationData formData = FormNotificationData.getInstance();
    private Utility utility = new Utility();

    private Button btnNext;

    private Spinner spinner;
    private SpinAdapterDirections spinAdapter;

    // Override methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_send_notification03_direction);

        btnNext = (Button) findViewById(R.id.form_send_notification_btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(validateForm()){
                    Intent intent = new Intent(v.getContext(), FormSendNotification04ContactDetails.class);
                    startActivity(intent);
                    overridePendingTransition(R.animator.push_left_in, R.animator.push_left_out);
                }
            }
        });

        // TODO: Populate directions based on database
        Direction[] directions = new Direction[10];

        directions[0] = new Direction(-1, getString(R.string.form_send_notification_dir_direction_hint));
        directions[1] = new Direction(1, "N");
        directions[2] = new Direction(2, "NE");
        directions[3] = new Direction(3, "E");
        directions[4] = new Direction(4, "SE");
        directions[5] = new Direction(5, "S");
        directions[6] = new Direction(6, "W");
        directions[7] = new Direction(7, "NW");
        directions[8] = new Direction(8, "Staying put");
        directions[9] = new Direction(9, "Not sure");

        spinAdapter = new SpinAdapterDirections(FormSendNotification03Direction.this,
                android.R.layout.simple_spinner_item,
                directions);
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner = (Spinner) findViewById(R.id.form_send_notification_dir_direction_sel);
        spinner.setAdapter(spinAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        double duration = formData.getDuration();
        makeToast("Duration: " + duration);
    }

    // Other Methods
    private int getDirectionIdFromSpinner(int pos){
        int selId = getDirectionFromSpinner(pos).getId();
        return selId;
    }

    private Direction getDirectionFromSpinner(int pos){
        return spinAdapter.getItem(pos);
    }

    private boolean validateForm(){
        boolean result = true;
        Direction dir;

        // Check spinner pos
        int selectedId  = getDirectionIdFromSpinner(spinner.getSelectedItemPosition());

        switch(selectedId){
            case -1:
                makeToastError(getString(R.string.error_msg_invalid_direction));
                result = false;
                break;
            default:
                dir = getDirectionFromSpinner(spinner.getSelectedItemPosition());
                formData.setDirection(dir);
                break;
        }

        return result;
    }
}
