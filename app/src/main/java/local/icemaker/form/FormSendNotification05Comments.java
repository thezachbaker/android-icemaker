package local.icemaker.form;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import local.icemaker.R;
import local.icemaker.helper.Utility;
import local.icemaker.model.Contact;
import local.icemaker.model.FormNotificationData;

public class FormSendNotification05Comments extends FormActivityBase {

    private FormNotificationData formData = FormNotificationData.getInstance();
    private Utility utility = new Utility();

    private EditText fieldComments;
    private Button btnSubmit;

    // Override methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_send_notification05_comments);

        fieldComments = (EditText) findViewById(R.id.form_send_notification_comments);
        btnSubmit = (Button) findViewById(R.id.form_send_notification_btn_submit);

        Contact contact = formData.getContact();
        makeToast(contact.getFirstName() + " " + contact.getLastName());

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateForm()){
                    if(utility.sendSms(getApplicationContext(), formData.createSms())){
                        // SMS was successful, send user home
                        finish();
                    } else {
                        // Fail silently, stay in form.
                    }
                }
            }
        });
    }

    // Methods
    public boolean validateForm(){
        String providedComments = fieldComments.getText().toString();
        formData.setComment(providedComments);
        return true;
    }
}
