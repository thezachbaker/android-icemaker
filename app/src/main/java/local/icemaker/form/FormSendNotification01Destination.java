package local.icemaker.form;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import local.icemaker.R;
import local.icemaker.adapter.SpinAdapterDestinations;
import local.icemaker.model.Destination;
import local.icemaker.model.DestinationArea;
import local.icemaker.model.FormNotificationData;

public class FormSendNotification01Destination extends FormActivityBase {

    private FormNotificationData formData = FormNotificationData.getInstance();
    private Button btnNext;
    private TextView txtDestinationArea;
    private RelativeLayout wrapperDestDetails;
    private EditText fieldDestDetails;

    private Spinner spinnerDestinations;
    private SpinAdapterDestinations spinAdapter;

    private String destAreaVal;

    // Override methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_send_notification01_destination);

        // UI Refs
        btnNext = (Button) findViewById(R.id.form_send_notification_btn_next);
        txtDestinationArea = (TextView) findViewById(R.id.form_send_notification_dest_destination);
        wrapperDestDetails = (RelativeLayout) findViewById(R.id.form_send_notification_wrapper_dest_details);
        fieldDestDetails = (EditText) findViewById(R.id.form_send_notification_dest_destination_other);

        // Set static values
        // TODO: Get DestinationArea name from DB
        formData.setDestinationArea(new DestinationArea("Alderaan National Park"));
        txtDestinationArea.setText(formData.getDestinationArea().getName());

        // Listeners
        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(validateForm()){
                    Intent intent = new Intent(v.getContext(), FormSendNotification02Duration.class);
                    startActivity(intent);
                    overridePendingTransition(R.animator.push_left_in, R.animator.push_left_out);
                }
            }
        });

        // TODO: Get Destinations from DB
        // Spinner implementation
        Destination[] destinations = new Destination[5];

        destinations[0] = new Destination(-1, "Choose a destination...");
        destinations[1] = new Destination(1, "Crater X Trailhead");
        destinations[2] = new Destination(2, "Rock Belt Creek");
        destinations[3] = new Destination(3, "Hopeless Canyon");
        destinations[4] = new Destination(0, "Other");

        spinAdapter = new SpinAdapterDestinations(FormSendNotification01Destination.this,
                android.R.layout.simple_spinner_item,
                destinations);
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerDestinations = (Spinner) findViewById(R.id.form_send_notification_dest_destination_sel);
        spinnerDestinations.setAdapter(spinAdapter);
        spinnerDestinations.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int selectedId = getDestinationIdFromSpinner(position);

                switch(selectedId){
                    case -1:
                        break;
                    case 0:
                        wrapperDestDetails.setVisibility(View.VISIBLE);
                        break;
                    default:
                        wrapperDestDetails.setVisibility(View.INVISIBLE);
                        fieldDestDetails.setText("");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // Other methods
    private boolean validateForm(){
        boolean result = true;
        Destination dest;

        // Check spinner pos
        int selectedId  = getDestinationIdFromSpinner(spinnerDestinations.getSelectedItemPosition());

        switch(selectedId){
            case -1:
                makeToastError(getString(R.string.error_msg_invalid_dest));
                result = false;
                break;
            case 0:
                String providedDestDetails = fieldDestDetails.getText().toString();

                if(providedDestDetails.length() < 1){
                    makeToastError(getString(R.string.error_msg_invalid_dest_details));
                    result = false;
                }

                dest = getDestinationFromSpinner(spinnerDestinations.getSelectedItemPosition());
                dest.setAuxName(providedDestDetails);
                formData.setDestination(dest);
                break;
            default:
                dest = getDestinationFromSpinner(spinnerDestinations.getSelectedItemPosition());
                formData.setDestination(dest);
                break;
        }

        return result;
    }

    private int getDestinationIdFromSpinner(int pos){
        int selId = getDestinationFromSpinner(pos).getId();
        return selId;
    }

    private Destination getDestinationFromSpinner(int pos){
        return spinAdapter.getItem(pos);
    }
}
