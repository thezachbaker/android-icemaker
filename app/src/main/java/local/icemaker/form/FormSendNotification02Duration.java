package local.icemaker.form;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import local.icemaker.R;
import local.icemaker.model.Destination;
import local.icemaker.model.FormNotificationData;

public class FormSendNotification02Duration extends FormActivityBase {

    private FormNotificationData formData = FormNotificationData.getInstance();

    private EditText txtDuration;
    private Button btnNext;

    // Override methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_send_notification02_duration);

        // Get UI refs
        btnNext = (Button) findViewById(R.id.form_send_notification_btn_next);
        txtDuration = (EditText) findViewById(R.id.form_send_notification_duration_duration);

        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(validateForm()){
                    Intent intent = new Intent(v.getContext(), FormSendNotification03Direction.class);
                    startActivity(intent);
                    overridePendingTransition(R.animator.push_left_in, R.animator.push_left_out);
                }
            }
        });

        Destination dest = formData.getDestination();
        makeToast("Destination: " + dest.getName() + "\r\n" + "Aux Name: " + dest.getAuxName());
    }

    // Other Methods
    private boolean validateForm(){
        boolean result = true;
        String providedDuration = txtDuration.getText().toString();

        if(providedDuration == null || providedDuration.isEmpty()){
            makeToastError(getString(R.string.error_msg_invalid_duration));
        } else {
            formData.setDuration(Double.parseDouble(providedDuration));
        }

        return result;
    }
}
