package local.icemaker.form;

import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import local.icemaker.R;
import local.icemaker.helper.Utility;

public class FormActivityBase extends AppCompatActivity {
    private Utility utility = new Utility();

    // Override methods
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form_send_notification01_destination, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.animator.push_right_in, R.animator.push_right_out);
    }

    // Custom methods
    protected void makeToast(String msg){
        utility.makeToast(this, msg);
    }
    protected void makeToastError(String msg){
        makeToast(getString(R.string.error_msg_prefix) + "\r\n" + msg);
    }


}
