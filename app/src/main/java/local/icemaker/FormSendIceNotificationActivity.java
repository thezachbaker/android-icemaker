package local.icemaker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FormSendIceNotificationActivity extends AppCompatActivity {

//    Button btnSend;
//    EditText txtPhoneNum;
//    EditText txtTrailName;

    Button btnNext;
//    int currentFormStage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_send_ice_notification);

//        currentFormStage = 1;
//
//        bindFormAdvanceBtn();

//        btnNext = (Button) findViewById()

//        btnSend = (Button) findViewById(R.id.form_create_notification_btn_submit);
//        txtPhoneNum = (EditText) findViewById(R.id.form_create_notification_contact_phone);
//        txtTrailName = (EditText) findViewById(R.id.form_create_notification_trail_name);
//
//        btnSend.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                String outPhoneNum = txtPhoneNum.getText().toString();
//                String outMsg = "I'm going on a hike at " + txtTrailName.getText().toString() + ".";
//
//                try{
//                    SmsManager smsManager = SmsManager.getDefault();
//                    smsManager.sendTextMessage(outPhoneNum, null, outMsg, null, null);
//                    Toast.makeText(getApplicationContext(), "ICE Notification Sent Successfully!",
//                            Toast.LENGTH_LONG).show();
//                } catch (Exception e){
//                    Toast.makeText(getApplication(),
//                            "Unable to send ICE notification :(",
//                            Toast.LENGTH_LONG).show();
//                    e.printStackTrace();
//                }
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form_send_ice_notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    public void loadFormView() {
//        if(currentFormStage <= 5 || currentFormStage >= 1){
//            currentFormStage++;
//
//            switch(currentFormStage){
//                case 2:
//                    setContentView(R.layout.form_send_notification_stg2_duration);
//                    break;
//                case 3:
//                    setContentView(R.layout.form_send_notification_stg3_direction);
//                    break;
//                case 4:
//                    setContentView(R.layout.form_send_notification_stg4_contact_details);
//                    break;
//                case 5:
//                    setContentView(R.layout.form_send_notification_stg5_comments);
//                    break;
//            }
//
//            bindFormAdvanceBtn();
//        }
//    }

//    public void bindFormAdvanceBtn(){
//        btnNext = (Button) findViewById(R.id.form_send_notification_btn_next);
//        btnNext.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v){
//                loadFormView();
//            }
//        });
//    }
}
